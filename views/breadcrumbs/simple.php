<?php if ( ! empty($breadcrumbs)): ?>
<ul class="breadcrumbs">
<?php foreach ($breadcrumbs as $crumb): ?>
	<li><a href="<?php echo $crumb->url() ?>"><?php echo $crumb->title(); ?></a></li>
<?php endforeach; ?>
</ul>
<?php endif; ?>
