<?php
if ( ! empty($breadcrumbs)):
	$title = '';
	while ($crumb = array_pop($breadcrumbs)):
		$title .= ' ‹ '.$crumb->title();
	endwhile;
	echo mb_substr($title, 3);
endif;