<?php defined('SYSPATH') OR die('No direct access allowed.');

class Breadcrumb {

	/**
	 * @var string Breadcrumb title
	 */
	protected $_title;

	/**
	 * @var string Breadcrumb url
	 */
	protected $_url;

	/**
	 * Helper class for chaining
	 * @param  string $title Breadcrumb title
	 * @param  string $url   Breadcrumb url
	 * @return Breadcrumb        Breadcrumb instance
	 */
	public static function factory($title = '', $url = '')
	{
		return new Breadcrumb($title, $url);
	}

	public function __construct($title = '', $url = '')
	{
		$this->_title = $title;
		$this->_url = $url;
	}

	/**
	 * Title getter setter
	 * @param  string  Title
	 * @return string|Breadcrumb
	 */
	public function title($title = NULL)
	{
		if ($title === NULL)
		{
			return $this->_title;
		}

		$this->_title = $title;
		return $this;
	}

	/**
	 * URL getter setter
	 * @param  string  URL
	 * @return string|Breadcrumb
	 */
	public function url($url = NULL)
	{
		if ($url === NULL)
		{
			return $this->_url;
		}

		$this->_url = $url;
		return $this;
	}

}