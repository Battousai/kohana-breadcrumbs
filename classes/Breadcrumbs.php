<?php defined('SYSPATH') OR die('No direct access allowed.');

class Breadcrumbs {

	/**
	 * Breadcrumb container
	 * @var array
	 */
	protected $_breadcrumbs = array();

	/**
	 * Helper method for chaining only
	 * @param  Breadcrumb $breadcrumb optional parameter with Breadcrumb type instance
	 * @return Breadcrumbs Breadcrumbs instance
	 */
	public static function factory(Breadcrumb $breadcrumb = NULL)
	{
		return new Breadcrumbs($breadcrumb);
	}

	public function __construct(Breadcrumb $breadcrumb = NULL)
	{
		$breadcrumb !== NULL and $this->add($breadcrumb);
	}

	/**
	 * Adds a Breadcrumb to the container
	 * @param Breadcrumb $breadcrumb A Breadcrumb instance
	 * @return Breadcrumbs Breadcrumbs instance
	 */
	public function add(Breadcrumb $breadcrumb)
	{
		$this->_breadcrumbs[] = $breadcrumb;
		return $this;
	}

	/**
	 * Clears breadcrumbs container
	 * @return Breadcrumbs Breadcrumbs instance
	 */
	public function clear()
	{
		$this->_breadcrumbs = array();
		return $this;
	}

	public function render($template = 'breadcrumbs/simple')
	{
		return View::factory($template)
			->set('breadcrumbs', $this->_breadcrumbs)
			->render();
	}

	public function __toString()
	{
		return $this->render();
	}

}